# catstagram

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

##Cómo visualizar la solución
1. Clonar repositorio
2. cd a la carpeta del proyecto
3. Ejecutar **npm install**
4. Ejecutar **npm run serve**
5. En el equipo que se ejecuta el proyecto: **localhost:8080**
6. En equipos remotos (smartphone, tableta, pc): **direccionipdelequipo:8080**
7. Enjoy!!! 😄

## ¿qué tecnología/herramientas utilizarías para guardar los favoritos de cada visitante de manera tal que cada browser tenga una lista distinta de favoritos?
  - Almacenaría directamente los objetos seleccionados en **store.js** creando su respectiva acción y mutacción
  - Almacenaría los datos en **localStorage** para no perder los datos después de un refresh
  - Para revisar si ya está guardado una imágen como favorita, haría una comparación del id del objeto seleccionado con los almacenados en el *state* usando map/filter y si arroja true mostrar un ícono indicando que ya está en favs.
  - *Usaría la misma estrategia del punto anterior para remover una imagen de favoritos.*

## Sin importar si se implementó el módulo de imágenes favoritos explica qué tecnologías (AWS, Firebase) podrías utilizar para que un usuario pueda tener una lista de favoritos que pueda llevar a distintas sesiones de navegador (por ejemplo, si abre hoy en una computadora y mañana en otra, que la lista de favoritos de ese usuario sea igual) 
  - Utilizaría Firebase para crear crear la lista de favoritos.
  - Guardaría los datos de la imagen en la que ejecuta el evento del doble click en **Realtime Database** para fines de este test.
  - En store.js crearía una acción *getFavs* en el que se mande a llamar los datos cada vez que se actualice la base de datos con un nuevo favorito en tiempo real (Ver sección *'Listen for value events' https://firebase.google.com/docs/database/web/read-and-write*).
  - En la acción *getFavs* incluiría un commit **SET_FAVS** en el cual guardaría las imágenes favoritas en una nueva propiedad del objeto *state*.
  - Para revisar si ya está guardado una imágen como favorita, haría una comparación del id del objeto seleccionado con los almacenados en el *state* usando map/filter y si arroja true mostrar un ícono indicando que ya está en favs.
  - *Usaría la misma estrategia del punto anterior para remover una imagen de favoritos.*

## ¿Qué recomendaciones de SEO harías para que tu app sea un éxito en los buscadores? 
  - Agregar meta tags en el header de la app (HTML, FB, Twitter).
  - Agregar descripciones a las imágenes (alt=""), agregar buenas prácticas de accesibilidad.
  - Hacer uso apropiado de elementos semánticos en la esctructura de la app.
  - Reducir tamaño/calidad de imágenes para reducir el consumo de datos.
  - Optimizar assets aplicando lazy load, para que la app funcione con tiempos de espera mínimos.
  - Minimizar archivos de estilo y JS.
  - Evitar lo más posible redirecciones.
  - Aplicar políticas de caché eficientes.
