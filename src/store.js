import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
axios.defaults.headers.common['x-api-key'] = "e258e272-84da-4674-88ad-c745cad6da6f";

export default new Vuex.Store({
  state: {
    breeds: [],
    imgs: [],
    loadingStatus: 'notLoading',
    queryParams: {
      limit: 12,
      order: 'asc',
      page: 0,
      breed_ids: '',
    },
    pagination_count: 0,
  },
  mutations: {
    SET_BREEDS: function(state, breeds){
      state.breeds = breeds;
    },
    NEW_IMGS: function(state, imgs){
      state.imgs = imgs;
    },
    SET_IMGS: function(state, imgs){
      state.imgs.push(...imgs);
    },
    SET_LOADING_STATUS: function(state, status){
      state.loadingStatus = status;
    },
    SET_QUERY_PAGE: function(state, page){
      state.queryParams.page = page;
    },
    SET_QUERY_BREED: function(state, breed){
      state.queryParams.breed_ids = breed;
    },
    SET_PAGINATION_COUNT: function(state, count){
      state.pagination_count = count;
    }
  },
  actions: {
    getImgs: async function({ commit, state }, queryParams){
      try{
        commit('SET_LOADING_STATUS', 'loading');
        let response = await axios.get('https://api.thecatapi.com/v1/images/search',
          {
            params: queryParams.params
          }
        );
        if(queryParams.action === 'push'){
          commit('SET_IMGS', response.data);
        }else{
          commit('NEW_IMGS', response.data);
        }
        commit('SET_PAGINATION_COUNT', response.headers['pagination-count'])
        commit('SET_QUERY_PAGE', (state.queryParams.page+1));
        commit('SET_LOADING_STATUS', 'done');
      }catch(err){
        commit('SET_LOADING_STATUS', 'error');
        console.log(err);
      }
    },
    getBreeds: async function({ commit }){
      try{
        commit('SET_LOADING_STATUS', 'loading');
        let response = await axios.get('https://api.thecatapi.com/v1/breeds/');
        commit('SET_BREEDS', response.data);
        commit('SET_LOADING_STATUS', 'done');
      }catch(err){
        commit('SET_LOADING_STATUS', 'error');
        console.log(err);
      }
    },
    fetchBreed: async function({ commit, dispatch, state }, breed){
      try{
        commit('SET_LOADING_STATUS', 'loading');
        commit('SET_QUERY_BREED', breed);
        commit('SET_QUERY_PAGE', 0);
        await dispatch('getImgs', {params: state.queryParams, action: 'new'});
      }catch(err){
        commit('SET_LOADING_STATUS', 'error');
        console.log(err);
      }
    }
  },
  getters: {
    getAllBreeds: state => {
      return state.breeds;
    },
    getAllImgs: state => {
      return state.imgs;
    },
    getNumPages: state => {
      let numPages = Math.ceil(state.pagination_count / state.queryParams.limit) | 0;
      return state.queryParams.page+1 > numPages;
    }
  }
})
